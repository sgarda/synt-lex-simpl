#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 17:39:49 2018

@author: Samuele Garda
"""

import os
import logging
import argparse
from kenlm import Model as LanguageModel
from gensim.utils import simple_preprocess
from simplifier import LexicalSimplifier
from train.train_lsa import SvdModel # need to import this for unpickling SvDModel object
from utils.prepare_shootout import preprocess
from utils.partial_beam_search import PartialBeamSearch
from evaluation.evaluation import evaluate_sub_gen,evaluate_lex_simpl

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 

def parse_arguments():
  """
  Parse command line arguments.
  """
  
  parser = argparse.ArgumentParser(description='Run lexical simplification experiments')
  parser.add_argument('--dir',required = True, type = str, help = "Directory where model is stored")
  parser.add_argument('--model-type',required = True,choices = ('w2v','lsa','poin','mixed'), type = str, help = "Path where all models are stored")
  parser.add_argument('--mode',default = 'most_similar',choices = ('most_similar', 'hierarchy','both'),type = str, help = "Mode for retrieving simplification candidates. \
                      Ignored for models : w2v,lsa")
  parser.add_argument('--topn',default = 10,type = int, help = "How many candidate to consider for simplification")
  parser.add_argument('--wiki-freq',required = True,type = str, help = "wikipedia freq folder")
  parser.add_argument('--alpha',default = 5e-5,type = float, help = "Probability threshold for simplification with std Wiki frequencies")
  parser.add_argument('--simple-alpha',default = 5e-5,type = float, help = "Probability threshold for simplification with Simple Wiki frequencies")
  parser.add_argument('--lm',type = str, required = True, help = "Path to language model")
  parser.add_argument('--beam-width',default = 5,type = int, help = "Beam width for Partial beam search")
  parser.add_argument('--eval',default = None,type = str, help = "Path to BenchLS. Evaluate systems. If not given run sentence example.")

  
  return parser.parse_args()

if __name__ == "__main__":
  
  args = parse_arguments()
  
  logger.info("Start main program")
  
  logger.info("Loaded Wiki Frequencies from `{}`".format(args.wiki_freq))
  
  wiki_freq = os.path.join(args.wiki_freq,'wiki_freq')
  simple_wiki_freq = os.path.join(args.wiki_freq,'simple_wiki_freq')
  
  simplifier = LexicalSimplifier(models_dir = args.dir, 
                                 model_type = args.model_type, 
                                 wiki_freq = wiki_freq, 
                                 simple_wiki_freq = simple_wiki_freq,
                                 alpha = args.alpha, 
                                 simple_alpha = args.simple_alpha,
                                 mode = args.mode,
                                 use_word_hier = True)
  
  logger.info("Create Lexical Simplifier using `{}` model with `{}` search method".format(args.model_type,args.mode))
  
  lm = LanguageModel(args.lm)
  
  logger.info("Loaded Language model : {}".format(args.lm))
  
  
  
  if args.eval:
    
    logger.info("Start evaluation process")
    
    evaluate_sub_gen(eval_path = args.eval,simplifier = simplifier,topn = args.topn)
#    evaluate_lex_simpl(eval_path = args.eval,simplifier = simplifier,topn = args.topn, lm = lm)
    
  else:
    
    logger.info("Start example")
    
    test = 'Given the minimalist (repetitive) nature of the music, audience members are free to enter and leave the opera as they wish.'

    
    original = simple_preprocess(test)
    processed = preprocess(test)
    
    logger.info("Original sentence : {}".format(test))
      
    BeamSearch = PartialBeamSearch(lm_model = lm)
    
    logger.info("Start partial beam search")
    
    best_hypo = BeamSearch.search_with_lm(original,processed,simplifier,topn = args.topn, beam_width = args.beam_width)
    
    logger.info("Most likely simplified sentence : {}".format(best_hypo))

    
 
      
   
        
        
  
    

  
  
  
    

    
    
    
    
                    

 
