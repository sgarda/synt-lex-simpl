#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 26 16:56:37 2018

@author: Samuele Garda
"""

import time
import os
import logging
import gensim
from gensim import utils,matutils
import numpy
import argparse
import scipy
from sys import maxsize
#import pyximport; pyximport.install(pyimport = True,setup_args={'include_dirs': numpy.get_include()})
from train.cooccur_matrix import get_cooccur


logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 

DIM = 300
DOC_LIMIT = None  # None for no limit
TOKEN_LIMIT = 50000
WINDOW = 10
DYNAMIC_WINDOW = False

def parse_arguments():
  """
  Parse command line arguments.
  """
  
  parser = argparse.ArgumentParser(description='Train LSA embeddings')
  parser.add_argument('--data', required = True, type = str, help = "Where training data is stored")
  parser.add_argument('--raw-mode', choices = ('numpy','scipy'),
                      default = 'numpy', type = str, help = "How to compute cooccurence matrix. `numpy` for cython, `scipy` for sparse matrix")
  parser.add_argument('--save', required = True, type = str, help = "Folder where to store embeddings")
  parser.add_argument('--keep', default = None, type = str, help = "Path to list of words to be kept. One word per line")  
  
  return parser.parse_args()

def sparse_raw2ppmi(cooccur):
  """
  Generating Positive Pointwise Mutual Information weighted space. 
  All operations are performed in place to avoid making copy of huge matrix. 
  Input matrix should be in `csr` format for speed in computation.

  :math:`ppmi(w,c)= pmi(w,c) \\text{ if } pmi(w,c)\\geq 0 \\text{ else } 0`
  
  where
  
  :math:`pmi(w,c) = log\\frac{\\text{ freq(w,c)*D }}{\\text{freq(w)freq(c)}}`
  
  :params:
    cooccur (scipy.sparse.csr_matrix) : raw counts cooccurrence matrix
    
  :return:
    cooccur (scipy.sparse.csr_matrix) : cooccurrence matrix weighted with PPMI 
  
  """
  start = time.time()
  
  logger.info("\nTransforming space into PPMI weighted co-occurence matrix\n")
  
  row_indices, col_indices = cooccur.nonzero()
  
  # #(w, c) / #w
  # Dividing all co-occurrence frequencies (rows) by the sum of frequency of single word
  marginal_words = numpy.array(cooccur.sum(axis=1))[:,0]
  cooccur.data /= marginal_words[row_indices] 
  
  # #(w, c) / (#w * #c)
  # Then dividing all co-occurrence frequencies by the sum of frequency of single context
  marginal_context = numpy.array(cooccur.sum(axis=0))[0,:]
  cooccur.data /= marginal_context[col_indices] 
  
  # #(w, c) * D / (#w * #c)
  # Normalizing the counts by the total number of occurrences D
  cooccur.data *= marginal_words.sum()  #(w, c) * D / (#w * #c)
  
  # PMI = log(#(w, c) * D / (#w * #c))
  # Taking the log for PMI
  cooccur.data = numpy.log(cooccur.data)
  
  # PPMI = max(0, log(#(w, c) * D / (#w * #c)))       
  # If values < 0, clip to zero, else leave unchanged
  cooccur.data = scipy.clip(cooccur.data,0.0,maxsize) # PPMI = max(0, log(#(w, c) * D / (#w * #c)))
  
  end = time.time()
  
  logger.info("\nTransformed original matrix into PPMI weighted co-occurence matrix in {0} s\n".format(round(end -start,5)))
  
  return cooccur


def raw2ppmi(cooccur):
    """
    Generating Positive Pointwise Mutual Information weighted space. 
    All operations are performed in place to avoid making copy of huge matrix.
    
    :params:
      cooccur (numpy.ndrray) : raw counts cooccurrence matrix
      
    :return:
      cooccur (numpy.ndrray) : cooccurrence matrix weighted with PPMI 
    """
    logger.info("computing PPMI on co-occurence counts")

    # following lines a bit tedious, as we try to avoid making temporary copies of the (large) `cooccur` matrix
    marginal_word = cooccur.sum(axis=1)
    marginal_context = cooccur.sum(axis=0)
    cooccur /= marginal_word[:, None]  # #(w, c) / #w
    cooccur /= marginal_context  # #(w, c) / (#w * #c)
    cooccur *= marginal_word.sum()  # #(w, c) * D / (#w * #c)
    numpy.log(cooccur, out=cooccur)  # PMI = log(#(w, c) * D / (#w * #c))

    logger.info("clipping PMI scores to be non-negative PPMI")
    cooccur.clip(0.0, out=cooccur)  # PPMI = max(0, log(#(w, c) * D / (#w * #c)))

    return cooccur
  
  

class VectorSpace(object):
  """
  Wrapper class for Vector Space model.
  Defines `most_similar` method to query vector space model with cosine similarity.
  """
  
  def most_similar(self, positive=[], negative=[], topn=10):
    """
    Find the top-N most similar words. Positive words contribute positively towards the
    similarity, negative words negatively.
    `model.word_vectors` must be a matrix of word embeddings (already L2-normalized),
    and its format must be either 2d numpy (dense) or scipy.sparse.csr.
    
    If only positive word is passed standard query. 
    
    Use positive and negative for analogy task.
    
    :param:
      positive (list) : List of words that contribute positively
      negative (list) : List of words that contribute negatively
      topn (int) number of recommendations
      
    :return:
      result (list) : most similar words with correspondent cosine similarity
    """
    if positive is None:
      positive = []
    if negative is None:
      negative = []
        
    if isinstance(positive, str) and not negative:
        # allow calls like most_similar('dog'), as a shorthand for most_similar(['dog'])
        positive = [positive]

    # add weights for each word, if not already present; default to 1.0 for positive and -1.0 for negative words
    positive = [
        (word, 1.0) if isinstance(word, (str, numpy.ndarray)) else word
        for word in positive]
    negative = [
        (word, -1.0) if isinstance(word, (str, numpy.ndarray)) else word
        for word in negative]
    
    # compute the weighted average of all words
    all_words, mean = set(), []
    for word, weight in positive + negative:
        if isinstance(word, numpy.ndarray):
            mean.append(weight * word)
        elif word in self.word2id:
            word_index = self.word2id[word]
            mean.append(weight * self.word_vectors[word_index])
            all_words.add(word_index)
        else:
            raise KeyError("word '%s' not in vocabulary" % word)
    if not mean:
        raise ValueError("cannot compute similarity with no input")
    if scipy.sparse.issparse(self.word_vectors):
        mean = scipy.sparse.vstack(mean)
    else:
        mean = numpy.array(mean)
    mean = matutils.unitvec(mean.mean(axis=0)).astype(self.word_vectors.dtype)

    dists = self.word_vectors.dot(mean.T).flatten()
    if not topn:
        return dists
    best = numpy.argsort(dists)[::-1][:topn + len(all_words)]

    # ignore (don't return) words from the input
    result = [(self.id2word[sim], float(dists[sim])) for sim in best if sim not in all_words]

    return result[:topn]
    
    

class SvdModel(VectorSpace):
  """
  Class defining an Vector Space factorized with Truncated Svd.
  """
  def __init__(self, corpus, dim, id2word, s_exponent=0.0):
    """
    Instantiate new LSA model.
    
    Compute Truncated SVD on original Vector Space.
    
    :params:
      
      corpus (gensim.corpora.MmCorpus) : PPMI weighted vector space 
      dim (int) : number of dimensions to preserve
      id2word (dict) : lookup id -> word
      s_exponent (float) : scaling factor of singular values
    """
    logger.info("calculating truncated SVD")
    lsi = gensim.models.LsiModel(corpus, id2word=id2word, num_topics=dim, chunksize=1000)
    self.singular_scaled = lsi.projection.s ** s_exponent
    # embeddings = left singular vectors scaled by the (exponentiated) singular values
    self.word_vectors = lsi.projection.u * self.singular_scaled
    self.word2id = None
    self.id2word = id2word
  



if __name__ == "__main__":

  args = parse_arguments()
  
  outf = lambda prefix: os.path.join(args.save, prefix)
  
  keep_tokens = [w.replace('\n') for w in open(args.keep).readlines()]
    
  sentences = gensim.models.word2vec.LineSentence(args.data, limit = DOC_LIMIT)
  
  if os.path.exists(outf('word2id')):
      logger.info("Found dictionary! Loading...")
      word2id = utils.unpickle(outf('word2id'))
  else:
      logger.info("Dictionary not found! Creating...")
      id2word = gensim.corpora.Dictionary(sentences, prune_at = 2000000)
      id2word.filter_extremes(keep_n=TOKEN_LIMIT, no_below = 2, keep_tokens = keep_tokens)  # filter out too freq/infreq words
      word2id = {v : k for k, v in id2word.items()} 
      utils.pickle(word2id, outf('word2id'))
      
  id2word = gensim.utils.revdict(word2id) 
  
  corpus = lambda: ([word for word in sentence if word in word2id] for sentence in sentences)
  
  
  pmi_model = 'pmi_matrix_dense.mm' if args.raw_mode == 'numpy' else 'pmi_matrix_sparse.mm'
      
  if os.path.exists(outf('pmi')):
    logger.info("PMI model found! Loading")
    
    pmi_matrix = utils.unpickle(pmi_model)
  
  else:
    
    if args.raw_mode == 'numpy':
    
      if not os.path.exists(outf('pmi_matrix_dense.mm')):
        logger.info("PMI matrix not found! Creating...")
   
        if os.path.exists(outf('cooccur.npy')):
          logger.info("Raw cooccurrence matrix found! Loading...")
          raw = numpy.load(outf('cooccur.npy'))
          
        else:
          
          logger.info("Raw cooccurrence matrix not found! Creating with numpy...")
          raw = get_cooccur(corpus(), word2id, window=WINDOW, dynamic_window=False)
          numpy.save(outf('cooccur.npy'), raw)
          # store the SPPMI matrix in sparse Matrix Market format on disk
        
        gensim.corpora.MmCorpus.serialize(outf(pmi_model), gensim.matutils.Dense2Corpus(raw2ppmi(raw),documents_columns=False))
        del raw
          
    else:
     
      if not os.path.exists(outf('pmi_matrix_sparse.mm')):
        logger.info("PMI matrix not found! Creating...")
        
        if os.path.exists(outf('cooccur_sparse')):
          logger.info("Raw cooccurrence matrix found! Loading...")
          raw = scipy.sparse.load_npz(outf('cooccur_sparse'))
        else:
          
          # creat BoW representation and get cooccurencies by multuplying by the transpose
          
          logger.info("Creating generator of doc2bow")
          
          test_corpus = lambda : ([id2word.doc2bow(sent) for sent in sentences])
          
          logger.info("Raw cooccurrence matrix not found! Creating with scipy...\n")
          logger.info("Generating BoW matrix\n")
          gensim.corpora.MmCorpus.serialize(outf('bow.mm'), test_corpus(), progress_cnt=10000)
          logger.info("Loading BoW matrix\n")
          mm_corpus = gensim.corpora.MmCorpus(outf('bow.mm'))
          logger.info("Dot product : from doc2words -> word2words\n")
          doc_word_sparse = gensim.matutils.corpus2csc(mm_corpus).T.tocsr()
          sparse_mask = doc_word_sparse > 0
          raw = doc_word_sparse.T.dot(sparse_mask)
          logger.info("Saving raw cooccur in sparse format\n")
          scipy.sparse.save_npz(outf('cooccur_sparse'), raw)

        gensim.corpora.MmCorpus.serialize(outf(pmi_model), gensim.matutils.Sparse2Corpus(sparse_raw2ppmi(raw),documents_columns=False))
        del raw
        
  logger.info("Creating SVD model...")
  
  out_file = 'svd_dense' if args.raw_mode == 'numpy' else 'svd_sparse'
  
  model = SvdModel(gensim.corpora.MmCorpus(outf(pmi_model)),id2word = id2word, dim = DIM, s_exponent=0.0)
  model.word2id = word2id
  utils.pickle(model, outf(out_file))
        
        
        

