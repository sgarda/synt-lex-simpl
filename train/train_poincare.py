#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 16:34:46 2018

@author: Samuele Garda
"""

import os
import argparse
import logging
from gensim.models.poincare import PoincareModel, PoincareRelations
from gensim import utils


logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 


def parse_arguments():
  """
  Parse command line arguments.
  """
  
  parser = argparse.ArgumentParser(description='Train Poincarè embeddings')
  parser.add_argument('--data', required = True, type = str, help = "Where training data is stored")
  parser.add_argument('--save', required = True, type = str, help = "Path where to store embeddings")  
  parser.add_argument('--epochs', default = 50, type = int, help = "Epochs to train")
  parser.add_argument('--batch-size', default = 50, type = int, help = "Batch size")
    
  
  return parser.parse_args()

if __name__ == "__main__":
  
  args = parse_arguments()
  
  C = 10
  BATCH_SIZE = args.batch_size
  NEGATIVE = 20
  SIZE = 100
  BURN_IN = 10
  REG = 1
  EPOCHS = args.epochs
  
  filename, file_extension = os.path.splitext(args.data)
  
  outf = lambda prefix: os.path.join(args.save, prefix) + file_extension
  
  relations = PoincareRelations(file_path= args.data, delimiter='\t')
  
  if os.path.exists(outf('pmi')):
    logger.info("Poincarè model found! Loading")
    
    model = utils.unpickle(outf('poin'))
  
  else:
  
    model = PoincareModel(train_data=relations, size= SIZE, burn_in=BURN_IN, negative = NEGATIVE,
                          burn_in_alpha = 0.1/C, regularization_coeff = REG)
    
    
    model.train(epochs= EPOCHS, print_every=1000)
    
    pos = os.path.basename(args.data)
            
    utils.pickle(model,outf('poin'))