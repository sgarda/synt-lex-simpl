#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 26 11:52:48 2018

@author: Samuele Garda
"""

import os
import logging
import argparse
import gensim
from gensim import utils

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 


def parse_arguments():
  """
  Parse command line arguments.
  """
  
  parser = argparse.ArgumentParser(description='Train Word2Vec embeddings')
  parser.add_argument('--data', required = True, type = str, help = "Where training data is stored")
  parser.add_argument('--cores', default = 4, type = int, help = "Number of cores to be used")
  parser.add_argument('--save', required = True, type = str, help = "Folder where to store embeddings")  
  parser.add_argument('--keep', default = None, type = str, help = "Path to list of words to be kept. One word per line")  
  
  return parser.parse_args()



if __name__ == "__main__":
  
  args = parse_arguments()

  DIM = 300
  DOC_LIMIT = None  # None for no limit
  TOKEN_LIMIT = 50000
  WORKERS = args.cores
  WINDOW = 10
  NEGATIVE = 20 
  EPOCHS = 1
  

  keep_tokens = [w.replace('\n') for w in open(args.keep).readlines()]  
  
  outf = lambda prefix: os.path.join(args.save, prefix)
    
  sentences = gensim.models.word2vec.LineSentence(args.data, limit = DOC_LIMIT)
  
  if os.path.exists(outf('word2id')):
      logger.info("Found dictionary! Loading...")
      word2id = utils.unpickle(outf('word2id'))
  else:
      logger.info("Dictionary not found! Creating...")
      id2word = gensim.corpora.Dictionary(sentences, prune_at = 2000000)
      id2word.filter_extremes(keep_n=TOKEN_LIMIT, no_below = 2, keep_tokens = keep_tokens)  # filter out too freq/infreq words
      word2id = {v : k for k, v in id2word.items()} 
      utils.pickle(word2id, outf('word2id'))
    
  id2word = gensim.utils.revdict(word2id)
        
    
  corpus = lambda: ([word for word in sentence if word in word2id] for sentence in sentences)
          
  logger.info("Start training W2V model")
  model = gensim.models.Word2Vec(size=DIM, min_count=0, window=WINDOW, workers=WORKERS, hs=0, negative=NEGATIVE)
  model.build_vocab(corpus())
  model.train(corpus(), epochs = EPOCHS, total_examples=model.corpus_count)
  model.init_sims(replace=True)
  model.word2id = {w :  v.index  for w, v in model.wv.vocab.items()}
  model.id2word = utils.revdict(model.word2id)
  model.word_vectors = model.wv.syn0norm
  logger.info("Saving Word2Vec model")
  utils.pickle(model, outf('w2v'))
  
        

    
    
    
    
    
    