#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 10:58:09 2018

@author: Samuele Garda

Compile cython function to build cooccurrence matrix.
"""

import numpy
from distutils.core import setup
from Cython.Build import cythonize


setup(
    name = "Cooccurrence Matrix",
    ext_modules = cythonize("./train/cooccur_matrix.pyx", include_path = [numpy.get_include()]),
)
