#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 10:40:24 2018

@author: Samuele Garda
"""


#TODO 
# stemming instead of lemmatize for merging words with common lemma
# remove OOV words
# PYTHON2 ENCODING == SHIT 
# ADD PLUR/SING FOR NN, CONJUGATION FOR VB

import logging
import codecs

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')

def load_pwkp_dataset(path):
  """
  Load PWKP dataset : pairs of complex,simple sentences.
  
  :params:
    path (str) : path to dataset
  
  :return:
    complex_sents (list) : complex sentences
    simple_sents (list) : simple sentences
  """
  
  logger.info("Loading data from {}".format(path))
  
  lines = []
   
  comp_simp_line = []
    
  with codecs.open(path, encoding = 'utf-8') as infile:
    for line in infile:
      if line != '\n':
        comp_simp_line.append(line)
      else:
        lines.append(comp_simp_line)
        comp_simp_line = []
         
         
  lines = [line  if len(line) == 2 else [line[0],' '.join(line[1:])] for line in lines]
   
  assert all(len(line) == 2 for line in lines), "Ouch"
  
  complex_sents,simple_sents = zip(*lines)
  
  logger.info("Loaded {} senteces couples (complex,simple)".format(len(lines)))
   
  return complex_sents,simple_sents

    
  
  
