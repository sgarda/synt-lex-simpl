#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 10:37:47 2018

@author: Samuele Garda
"""

import re
from pattern.en import tenses,conjugate,pluralize, singularize

RM_POS = re.compile('\.[a-z]')

def is_plural(word):
  """
  Check if noun is in plural form or not.
  
  :param:
    word (str) : word 
  :return:
    plural (bool) : whether word is plural or not
  """
  singularForm = singularize(word)
  plural = True if word is not singularForm else False
  
  return plural

def rm_pos(w):
  """
  Remove PoS tag in WordNet format.
  
  `give.v` > `give`
  
  :params:
    w (str) : word
  
  :return:
    word without PoS tag
  """
    
  return re.sub(RM_POS,'',w)

def nouns_norm(orig,rec):
  """
  Pluralize subsitution candidates if original word was in plural form. Otherwise return word
  
  :params:
    orig (str) : original word
    rec (str) : substitution candidate
    
  :return:
    word (str) : subsitution candidates with same 'number' as original
  """
  
  rec_nopos = rm_pos(rec)
  
  word = pluralize(rec_nopos) if is_plural(orig) else rec_nopos
  
  return word


def verbs_norm(orig,rec):
  """
  Conjugate the substitution candidate to the same tense/person/modus as original word.
  
  :params:
    orig (str) : original word
    rec (str) : substitution candidate
  :return:
    conj_verb (str) : substitution verb conjugated as original
  """
  
  tense,person,number, mood, aspect = get_verb_aspects(orig)
  
  rec_nopos = rm_pos(rec)
  
  conj_verb = conjugate(verb = rec_nopos,
                        tense = tense,
                        person = person,
                        number = number,
                        mood = mood,
                        aspect = aspect) 
  
  return conj_verb

def get_verb_aspects(verb):
  """
  Retrieve tense/person/modus from a verb
  
  :params:
    verb (str) : verb
  
  :return:
    verb_aspects (tuple) : first possible combination of verb aspects (tense/person/modus)
  """
  
  verb_aspects = list(tenses(verb)[0])
    
  return verb_aspects


def normalize(orig,rec):
  """
  Wrapper function to normalize substitution candidates to original sentence word.
  
  For nouns and verbs normalization is applied, otherwise simply remove PoS tag from candidates.
  
  :params:
    orig (str) : original word
    rec (str) : substitution candidate
    
  :return:
    norm_word (str) : normalized word
  """
  
  if rec.endswith('.n'):
    
    norm_word = nouns_norm(orig,rec)
  
  elif rec.endswith('.v'):
    
    norm_word = verbs_norm(orig,rec)
    
  else:
    
    norm_word = rm_pos(rec)
    
  return norm_word
  
  
  