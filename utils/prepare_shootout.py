#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Radim Rehurek <me@radimrehurek.com>

"""
USAGE: %(program)s enwiki-latest-pages-articles.xml.bz2 OUTPUT_DIRECTORY

Parse all articles from a raw bz2 Wikipedia dump => train a latent semantic model on the \
articles => store resulting files into OUTPUT_DIRECTORY:

* title_tokens.txt.gz: raw article titles and tokens, one article per line, "article_title[TAB]space_separated_tokens[NEWLINE]"
* dictionary: mapping between word<=>word_id
* dictionary.txt: same as `dictionary` but in plain text format
* tfidf.model: TF-IDF model
* lsi.model: model for latent semantic analysis model, trained on TF-IDF'ed wiki dump
* lsi_vectors.mm: wikipedia articles stored as vectors in LSI space, in MatrixMarket format

The input wikipedia dump can be downloaded from http://dumps.wikimedia.org/enwiki/latest/

Example:
    ./prepare_shootout.py ~/data/wiki/enwiki-latest-pages-articles.xml.bz2 ~/data/wiki/shootout
"""

import os
import logging
import argparse
import multiprocessing
import bz2
import gensim
from smart_open import smart_open
from collections import defaultdict
from nltk import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer


logger = logging.getLogger('prepare_shootout')

STD_MIN_WORDS = 50  # ignore articles with fewer tokens (redirects, stubs etc)
SIMPLE_MIN_WORDS = 10
ALLOW_POS = ['v','n','a','r']
PENN2WN_POS_MAP = {'V' : 'v','N':'n','J' : 'a', 'R' : 'r'}
LEMMATIZER = WordNetLemmatizer()




def parse_args():
  """
  Parse command line arguments
  """
  
  parser = argparse.ArgumentParser(description='Create compressed train data from Wikipedia English and Simple Wikipedia English')
  parser.add_argument('--wiki', type = str, required = True, help = "Path to wikipedia dump (xml.bz2)")
  parser.add_argument('--simple-wiki', type = str, required = True, help = "Path to simple wikipedia dump (xml.bz2)")
  parser.add_argument('--out', type = str, required = True, help = "Directory where to store parsing result")
  parser.add_argument('--cores', type = int, default = 2, help = "Number of cores")
  parser.add_argument('--limit', type = int, default = int(1000000), help = "Limit of articles to be loaded")
  
  return parser.parse_args()


def penn2wn(pos_text):
  """
  Translate Penn PoS tags to WordNet one in sentence. If PoS not in simplifiable ones (adjectives,nouns,verbs,adverbs)
  mark as `nd`.
  
  :params:
    pos_text (list) : tokens in tuple form (token,PoS)
  :return:
    Sentece with pos tags in WordNet format (e.g. `VBZ` -> `v`)
  """
  
  return [(w[0],PENN2WN_POS_MAP.get(w[1][0],'nd')) for w in pos_text]


def wp_join(text):
  """
  Reduce tuple of pos_tagged tokens in list to stings for using as input vocabulary to vector space models.
  
  :params:
    text (list) : list of tokens
  :return:
    list of tokens with merged pos tags (e.g. `[(Dogs,n),(bark,v)] -> [dogs.n, bark.v]`)
  """
  
  return ['.'.join(w) for w in text]

def lemmatize(text):
  """
  Lemmatize tokens with wordnet lemmatizer
  
  :params:
    text (list) : list of tokens with postags in wordnet format
  :return:
    list of lemmatized tokens
  """
  
  return [(LEMMATIZER.lemmatize(w[0],w[1]),w[1]) if w[1] in ALLOW_POS else w for w in text]


def preprocess(text, pre_tok = False):
  """
  Apply preprocessing to text.
  
  :params:
    text (str) : text
  :return:
    lemmatized postagged list of tokens
    
  """
  
  text = gensim.utils.simple_preprocess(text) if pre_tok == False else text.split()
  
  text = wp_join(lemmatize(penn2wn(pos_tag(text))))
    
  return text
  
def process_article(wiki_page):
  """
  Parse a wikipedia article, returning its content as a list of tokens, all unicode.
  
  :params:
    wiki_page (tuple) : wikipedia article composed by : title,text and id
  
  :return:
    title (str) : article title
    text (list) : lemmatized postagged list of tokens
  """
  title, text, page_id = wiki_page
  
  title = gensim.utils.to_unicode(title.replace('\t', ' ')) 
    
  text = gensim.corpora.wikicorpus.filter_wiki(text)
  text = preprocess(text)
  
  return title,text 

def convert_wiki(infile, processes=multiprocessing.cpu_count(), source = 'wiki'):
  """
  Yield articles from a bz2 Wikipedia dump `infile` as (title, tokens) 2-tuples.

  Only articles of sufficient length are returned (short articles & redirects
  etc are ignored).

  Uses multiple processes to speed up the parsing in parallel.
  
  :params:
    infile (str) : path to bz2 Wikipedia dump
    processes (int) : number of cores to be used
    source (str) : type of wikipedia (Simple or Standard)

  """
  
  MIN_WORDS = STD_MIN_WORDS if source == 'wiki' else SIMPLE_MIN_WORDS
  
  logger.info("extracting articles from %s using %i processes" % (infile, processes))
  articles, articles_all = 0, 0
  positions = 0
  
  pool = multiprocessing.Pool(processes)
  # process the corpus in smaller chunks of docs, because multiprocessing.Pool
  # is dumb and would try to load the entire dump into RAM...
  texts = gensim.corpora.wikicorpus._extract_pages(bz2.BZ2File(infile))  # generator
  ignore_namespaces = 'Wikipedia Category File Portal Template MediaWiki User Help Book Draft'.split()
  for group in gensim.utils.chunkize(texts, chunksize=10 * processes):
    for title,tokens in pool.imap(process_article, group):
      if articles_all % 1000 == 0:
        logger.info("PROGRESS: at article #%i accepted %i articles with %i total tokens" % (articles_all, articles, positions))
      articles_all += 1

      # article redirects and short stubs are pruned here
      if any(title.startswith(ignore + ':') for ignore in ignore_namespaces) or len(tokens) < MIN_WORDS:
        continue
      
      # all good: use this article
      articles += 1
      positions += len(tokens)
      yield title,tokens
  pool.terminate()

  logger.info("finished iterating over Wikipedia corpus of %i documents with %i positions (total %i articles)" %(articles, positions, articles_all))

if __name__ == '__main__':

  os.system( "taskset -pc 0-1 %d > /dev/null" % os.getpid() )
  
  logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
  logging.root.setLevel(level=logging.INFO)
    
  args = parse_args()

  outfile = lambda fname: os.path.join(args.out, fname)
#  files = [args.simple_wiki]
  files = (args.wiki,args.simple_wiki)
  limit = args.limit
  sources = ['wiki','simple_wiki']
#  sources = ['simple_wiki']
  cores = args.cores
  
  counts = [defaultdict(int),defaultdict(int)]
  
  logger.info("Start processing files")
  # extract plain text from the XML dump
  preprocessed_file = outfile('complete.txt.gz')
  if not os.path.exists(preprocessed_file):
    with smart_open(preprocessed_file, 'wb') as fout:
      for idx,infile in enumerate(files):
        logger.info("Start converting `{}` articles".format(infile))
        for docno,(title,tokens) in enumerate(convert_wiki(infile, cores, source = sources[idx])):
          if docno <= limit:
            
            for tok in tokens:
              counts[idx][tok] += 1
            
            bytes_line = gensim.utils.to_utf8(' '.join(tokens))
            fout.write(bytes_line) # make sure we're storing proper utf8
          else:
            break
            print("Stopped")

  logger.info("Completed creation of input data stored at `{}`".format(preprocessed_file))
  logger.info("Saving english wiki frequencies at : `{}`".format(outfile('wiki_freq')))
  gensim.utils.pickle(counts[0],outfile('wiki_freq'))
  logger.info("Saving simple english wiki frequencies at : `{}`".format(outfile('simple_wiki_freq')))
  gensim.utils.pickle(counts[1],outfile('simple_wiki_freq'))
        
