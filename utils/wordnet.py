#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 15:42:35 2018

@author: Samuele Garda
"""
import re
import logging
import argparse
from nltk.corpus import wordnet as wn

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 

PATTERN = re.compile('\.\d{2}')

def rm_sense(w):
  
  return re.sub(PATTERN,'',w)

# TAKEN FROM FACEBOOK REPO
def create_transitive_closure(pos,out_path):
  """
  Create training data for Poicarrè embeddings.
  
  :param:
    pos (str) : which type of words to condider. Choices = (`n`,`v`,`a`,`r`)
    out_path (str) : where to store transitive closure relations
  """
  
  logger.info("Start traversing  all wordnet synsets for PoS : `{}`".format(pos))
  
  logger.info("This might take a while...")
  
  edges = set()
  
  if pos == 'n' or pos == 'v':
  
    for synset in wn.all_synsets(pos = pos):
      # write the transitive closure of all hypernyms of a synset to file
      for hyper in synset.closure(lambda s: s.hypernyms()):
  #      print("Direct")
  #      print("{}\n".format((synset.name(), hyper.name())))
        edges.add((rm_sense(synset.name()), rm_sense(hyper.name())))
    
      # also write transitive closure for all instances of a synset
      for instance in synset.instance_hyponyms():
        for hyper in instance.closure(lambda s: s.instance_hypernyms()):
          edges.add((rm_sense(instance.name()),  rm_sense(hyper.name()))) 
          for h in hyper.closure(lambda s: s.hypernyms()):
            edges.add((rm_sense(instance.name()),  rm_sense(hyper.name()))) 
  
  elif pos == 'a':
    
    sim = lambda s : s.similar_tos()
    
    for synset in wn.all_synsets(pos = pos):
      # write the transitive closure of all hypernyms of a synset to file
      for hyper in synset.closure(sim, depth = 1):
  #      print("Direct")
  #      print("{}\n".format((synset.name(), hyper.name())))
        edges.add((rm_sense(synset.name()).replace('.s','.a'), rm_sense(hyper.name()).replace('.s','.a')))
        
  elif pos == 'r':
  
    sim = lambda s : list(set([wn.synsets(lemma, 'r')[0] for lemma in s.lemma_names()]))
    
    for synset in wn.all_synsets(pos = pos):
      # write the transitive closure of all hypernyms of a synset to file
      for hyper in synset.closure(sim, depth = 1):
        if not synset.name().startswith(hyper.name().split('.')[0]):
          edges.add((rm_sense(synset.name()), rm_sense(hyper.name())))
    
    
  with open(out_path, 'w') as fout:
    for i, j in edges:
      fout.write('{}\t{}\n'.format(i,j))
  
  logger.info("Finished writing transitive closure relations  to `{}`".format(out_path))
  
  
def parse_arguments():
  """
  Parse command line arguments.
  """
  
  parser = argparse.ArgumentParser(description='Create training data for Poincarè embeddings')
  parser.add_argument('--pos', default = 'n', type = str, choices = ('n','v','a','r'),
                      help="Which type of words to consider in training: \
                      Nouns :`n`, Verbs :`v`,Adjectives : `a`, Adverbs : `r`")
  parser.add_argument('--out', required = True, type = str, help = "Where to store training data")
  
  return parser.parse_args()
  

if __name__ == "__main__":
  
  args = parse_arguments()
  
  create_transitive_closure(pos = args.pos, out_path = args.out)
  
  
  