#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 18:00:22 2018

@author: Samuele Garda
"""

import os
import re
from nltk.corpus import wordnet as wn
from gensim.utils import unpickle
from nltk.stem.lancaster import LancasterStemmer


class LexicalSimplifier(object):
  """
  Class of LexicalSimplifier.
  Main methods implemented:
    `get_candidates_*` : methods for retrieving substitution candidates of complex words
  """
  
  def __init__(self,models_dir,model_type,wiki_freq,simple_wiki_freq,alpha,simple_alpha,mode,use_word_hier = False):
    """
    Instatiate object.
    
    :params:
      model (gensim.models or train.train_lsa.VectorSpace) : a vector space model implementing `most_similar` method
      model_type (str) : type of model used. Choiches (`lsa`,`w2v`,`poin`)
      wiki_freq (dict) : frequencies of words in Wikipedia (used for training word vector models)
      simple_wiki_freq (dict) : frequencies of words in Simple Wikipedia (used for training word vector models)
      alpha (float) : threshold for deciding to simply based on word probability in Wikipedia
      simple_alpha (float) : threshold for deciding to simply based on word probability in Simple Wikipedia
      
    """
    self.models_dir = models_dir
    self.model_type = model_type
    self.wiki_freq = unpickle(wiki_freq)
    self.simple_wiki_freq = unpickle(simple_wiki_freq)
    self.use_word_hier = use_word_hier
    self.alpha = alpha
    self.simple_alpha = simple_alpha
    self.to_simpl = ('n','v','a','r','j')
    self.pat_rm_pos = re.compile('\.[a-z]')
    self.pat_rm_sense = re.compile('\.\d{2}')
    self.tot_count = sum([v for v in self.wiki_freq.values()])
    self.simple_tot_count = sum([v for v in self.simple_wiki_freq.values()]) 
    self.stemmer = LancasterStemmer()
    self.mode = mode
    self.dicted_models = {'lsa' : os.path.join(self.models_dir,'svd_dense'),
                          'w2v' : os.path.join(self.models_dir,'w2v'),
                          'poin' : {'n' : os.path.join(self.models_dir,'poin.nouns'),
                                    'a' : os.path.join(self.models_dir,'poin.adjs'),
                                    'r' : os.path.join(self.models_dir,'poin.advs'),
                                    'v' : os.path.join(self.models_dir,'poin.verbs')}}
    
    self.model = self.get_model()
    
    
  def get_model(self):
    """
    Retrive model based on model type decided. If `mixed` all models are picked.
    
    :params:
      dicted_models (dict) : model_name -> model_path lookup
      model_type (str) : type of model to be used for predictions
      
    :return:
      model (gensim.models) : Embeddings model
    """                   
    
    if self.model_type == 'lsa' or self.model_type == 'w2v':
      model = unpickle(self.dicted_models[self.model_type])
      
    elif self.model_type == 'poin':
      model = {k : unpickle(v)  for k,v in self.dicted_models[self.model_type].items()}
    elif self.model_type == 'mixed':
      model = {}
      for model_name,model_path in self.dicted_models.items():
        if not isinstance(model_path,dict):
          model[model_name] = unpickle(model_path)
        else:
          model[model_name] = {}
          for k,v in model_path.items():
            model[model_name][k] = unpickle(v)
            
    print(model)        
            
    return model
    
    
  def _rm_pos(self,w):
    """
    Remove PoS tag in WordNet format.
    
    `give.v` > `give`
    
    :params:
      w (str) : word
    
    :return:
      word without PoS tag
    """
    return re.sub(self.pat_rm_pos,'',w)
  
  def _rm_sense(self,w):
    """
    Remove sense from WordNet synset.
    
    `give.v.O1` > `give.v`
    
    :params:
      w (str) : word
    
    :return:
      word without synset sense
    """
    return re.sub(self.pat_rm_sense,'',w)  
  
  def _get_pos(self,w):
    """
    Get PoS of word
    
    :params:
      w (str) : word
      
    :return:
      word pos tag
      
    """
    
    return w.split('.')[-1]
    
    
  def share_lemma(self,w1,w2):
    """
    Check if two words have same stem.
    
    :params:
      w1 (str) : word1
      w2 (str) : word2
    :return:
      boolean for answering if words share lemma.
    """
    
    no_valid = True if self.stemmer.stem(w1) == self.stemmer.stem(w2) else False
    
    return no_valid
  
  def is_synonym(self,synsets_w1,w2):
    """
    Check if two words are synoyms in WordNet.
    
    :params:
      synsets_w1 (list) : list of synsets (all possible "senses") for word 1
      w2 (str) : word 2
    
    :return:
      valid (bool) :  answering if words are syonyms.
    """
    
    synonyms = set([lemma for syn in synsets_w1 for lemma in syn.lemma_names()])
        
    valid = True if self._rm_pos(w2) in synonyms else False
        
    return valid
    
  def is_hypernym(self,synsets_w1,w2,pos_w1):
    """
    Check if a word 1 is hyperonym of word 2. For noun and verbs it is possible to have a direct transitive closure set with
    the hypernym function.
    For adjectives and adverbs a similarity function is used. Although not 100% technically correct it has been show to be 
    accurate enough for the purposes of these task.
    
    :params:
      synsets_w1 (list) : list of synsets (all possible "senses") for word 1
      w2 (str) : word 2
      pos_w1 (str) : pos tag of word 1
      
    :return:
      valid (bool) :  answering if word 2 is hypenym of word 1.
      
    """
    
    if pos_w1 == 'n' or pos_w1 == 'v':
      
      sim = lambda s: s.hypernyms()
      
      hypernyms = [self._rm_sense(hyper.name()) for hyper in synsets_w1[0].closure(sim)]
            
    elif pos_w1 == 'a':
      
      sim = lambda s : s.similar_tos()
      
      hypernyms = [self._rm_sense(hyper.name()) for hyper in synsets_w1[0].closure(sim, depth = 1)]
            
    elif pos_w1 == 'r':
      
      sim = lambda s : list(set([wn.synsets(lemma, 'r')[0] for lemma in s.lemma_names()]))
      
      hypernyms = [self._rm_sense(hyper.name()) for hyper in synsets_w1[0].closure(sim, depth = 1)]
                    
    valid = True if w2 in hypernyms else False
    
    return valid
  
  
  def is_hyper_or_syn_onym(self,w1,w2):
    """
    Check if word 2 is hyperonym or synonym of word 1.
    
    :params:
      w1 (str) : word 1
      w2 (str) : word 2
    :return:
      valid (bool) :  answering if word 2 is hypenym or synobym of word 1.
    """
        
    synsets_w1 = wn.synsets(*w1.split('.'))
    
    pos_w1 = self._get_pos(w1)
    
    if not synsets_w1:
      
      return False
    
    else:
            
      valid = True if self.is_synonym(synsets_w1,w2) or self.is_hypernym(synsets_w1,w2,pos_w1) else False
        
      return valid
      
  def get_candidates_lsa_w2v(self,model,word,topn):
    """
    Get possible simpler substitution candidates for a word with Word2Vec or LSA embeddings.
    
    From `topn` most similar words are selected the one that:
      1) Have same PoS
      2) Do not share the lemma
      3) Are a hyperonym or a synonym of the word
  
    If the word is OOV or no subsitution candidates are left after filtering the original word is returned.
    
    :params:
      word (str) : word to be simplified
      topn (int) : number of most similar words to be filtered
      
    :return:
      candidates (list) : list of substitution candidates
      
    """
    try:
      raw_candidates = [w[0] for w in model.most_similar(word,topn = topn)]
    except KeyError:

      raw_candidates = []
      
    if raw_candidates:
      
      pos = self._get_pos(word)
      word_no_pos = self._rm_pos(word)
      
      cand = [w for w in raw_candidates if w.endswith(pos)]
      
      cand = [w for w in cand if not self.share_lemma(self._rm_pos(w),word_no_pos)]
      
      if self.use_word_hier:
        
        cand = [w for w in cand if not self.is_hyper_or_syn_onym(word,w)]

      candidates = cand

    else:
      
      candidates = [word]
      
    return candidates

  def get_candidates_poin(self,model,word,topn):
    """
    Get possible simpler substitution candidates for a word with Poincarè embeddings.
    If `mode` is "hierarchy" use possible chain of ancestors and descendants to retrieve substituion candidates.
    
    If the word is OOV or no subsitution candidates are left after filtering the original word is returned.
    
    :params:
      word (str) : word to be simplified
      topn (int) : number of subsitution candidates to be filtered
      mode (str) : Which subsitution candidates to pick. Most similalr in space or possibel hierachy.
      
    :return:
      candidates (list) : list of substitution candidates
    
    """
    
    try:
      
      candidates = []
      
      pos = self._get_pos(word)
      
      if self.mode == 'most_similar' or self.mode == 'both':
      
        candidates += [' '.join(word[0].split('_')) for word in model[pos].kv.most_similar(word,topn = topn) if not '_' in word[0]]
                
      elif self.mode == 'hierarchy' or self.mode == 'both':
        
        candidates += [ ' '.join(w.split('_')) for w in model[pos].kv.ancestors(word) if not '_' in w]
                
        candidates += [ ' '.join(w.split('_')) for w in model[pos].kv.descendants(word) if not '_' in w]
                      
      else:
        
        raise ValueError("The mode prediction {} for the Poincarè model is not a valid option! Please choose `most_similar` or `hierarchy`".format(self.mode))
      
    except KeyError:
      
      candidates = [word]

    return candidates
  
  
  def get_mixed_candidates(self,model,word,topn):
    """
    Get all possible simpler substitution candidates from all embeddings.
    
    If the word is OOV or no subsitution candidates are left after filtering the original word is returned.
    
    :params:
      word (str) : word to be simplified
      topn (int) : number of subsitution candidates to be filtered
      mode (str) : which subsitution candidates to pick. Most similalr in space or possibel hierachy. ONLY for Poincarè
      poin_both (bool) : use both `most_similar` and `hierachy`. ONLY for Poincarè.
      
    :return:
      candidates (list) : list of substitution candidates

    """
    
    lsa_cand = self.get_candidates_lsa_w2v(model.get('lsa'),word,topn)
        
    w2v_cand = self.get_candidates_lsa_w2v(model.get('w2v'),word,topn)
        
    poin_cand = self.get_candidates_poin(model.get('poin'), word,topn)
        
    candidates = lsa_cand + w2v_cand + poin_cand 
        
    return candidates
  
  
  
  def get_candidates(self,word,topn):
    """
    Wrapper function for methods generating substitution candidates. 
    
    :params:
      word (str) : word to be simplified
      topn (int) : number of subsitution candidates to be filtered
      mode (str) : which subsitution candidates to pick. Most similalr in space or possibel hierachy. ONLY for Poincarè
      poin_both (bool) : use both `most_similar` and `hierachy`. ONLY for Poincarè.
    
    :return:
      candidates (list) : list of substitution candidates
    """
    
    if self.model_type == 'lsa' or self.model_type == 'w2v':
      
      cand = self.get_candidates_lsa_w2v(self.model,word,topn)
      
    elif self.model_type == 'poin':
      
      cand = self.get_candidates_poin(self.model, word,topn)
      
    elif self.model_type == 'mixed':
      
      cand = self.get_mixed_candidates(self.model, word,topn)
      
    else:
      
      raise ValueError("Model type must be one of (`lsa`,`w2v`,`poin`,`mixed`)! Recieved : {}".format(self.model_type))
      
    
    return cand
     
  def _get_prob(self,word):
    """
    Compute probability of a word in Wikipedia or Simple Wikipedia. If word in Simple Wikipedia use its counts,
    otherwise Wikipedia ones. If word is OOV return 0 probability.
    
    :param:
      word (str) : word
    
    :return:
      alpha (float) : which threshold is used (Simple or Standard Wikipedia)
      prob (float) : word probability
    """
      
    if word in self.wiki_freq or word in self.simple_wiki_freq:
      
      if word in self.simple_wiki_freq:
        tot_count = self.simple_tot_count
        lookup = self.simple_wiki_freq
        alpha = self.simple_alpha
        
      elif word in self.wiki_freq:
        tot_count = self.tot_count
        lookup = self.wiki_freq
        alpha = self.alpha
        
      w_count = lookup.get(word,1e-12)
      
      prob = w_count / tot_count
            
    else:
      
      alpha = 0
      prob = 0
  
    return alpha,prob
  
  def should_simplify(self,word):
    """
    Core logic of LexicalSimplifier : decide whether to simplify a word or not.
    
    If the probabilty of seeing `word` is lower than a given threshold then the words get to be simplified.
    
    OOV words are not simplified by default.
    
    :params:
      word (str) : word to be simplied
      
    :return:
      Boolean specifying if words should be simplified or not.
    """
                
    alpha,prob = self._get_prob(word)
    
    if prob:
          
      if prob > alpha:
        
        print("Word `{}` is very comon, no need to simplify : {}".format(word,prob))
        
        return 0
      
      
      else:
        
        print("Word `{}`  need to be simplified : {}".format(word,prob))
                
        return 1
        
      
    else:
      
      print("Word `{}` is OOV, cannot proceed".format(word))
      
      return 0
      
  
  
#  def simplify_word(self,word,topn,mode):
#    
#    if self.model_type == 'poin':
#      
#      sub = self._simplify_poin(word,topn,mode)
#      
#    else:
#      
#      sub = self._simplify_lsa_or_w2v(word,topn)
#      
#    return sub
#  
#  def simplify_sentence(self,sent,topn,mode):
#    
#    new_sent = []
#    
#    for word in sent:
#      
#      print("Considering word : `{}`".format(word))
#      
#      if not word.endswith(self.to_simpl):
#        print("Word pos not simplifiable\n".format(word))
#        new_sent.append(word)
#      else:
#        
#        if self._should_simplify(word):
#         sub = self.simplify_word(word,topn,mode)
#         new_sent.append(sub)
#        
#        else:
#          new_sent.append(word)
#          
#      print("\n")
#    
#    return new_sent
  
  

      
    
