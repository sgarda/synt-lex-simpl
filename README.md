# Lexical Simplification

Submission for final project of "Automatic Text Simplification",Summer Semester 2018,University of Potsdam.
Exploring different word vector representation and filtering techniques to automatically simplify words in sentences.

The task involves the use of three different embeddings models:
- Latent Semantic Analysis 
- Word2Vec
- Poincarè embeddings

## Install 

Download or clone the git repo https://github.com/sgarda/synt-lex-simpl

## Embeddings

When creating the embeddings models please be sure to place all of them **in the same folder**.

### LSA and W2V

#### Extract Wikipedia and Simple English Wikipedia articles

In order to use the lexical simplifier you will first need to generate the word embeddings.

For Word2Vec and LSA embeddings yout will need to dowload the english wikipedia dump and the simple english wikipedia dump.

    wget enwiki-latest-pages-articles.xml.bz2
    wget simplewiki-latest-pages-articles.xml.bz2  
    
Although `gensim` library will work directly with the compressed file you will need ~15GB of free memory.

Once you have these files you need to create a single file where the preprocessed corpus is stored.
    
    python3 -m utils.prepareshootout --wiki  --simple-wiki --out 

This scripts will use `nlkt.pos_tag` and `nltk.WordNetLemmatizer` to annotate each token and will save the frequency of each word in two separate files (`out_folder/wiki_freq`,`out_folder/simple_wiki_freq`). You will need these files later.
File processing can be performed in parallel specifying the amounts of `--cores`. This step is high demanding in terms of computation, thus if the resources are limited it is possible to set a limit of the amount of articles to be processed with `--limit`. The time required by this step depends on the number of available CPUs. For ~1 milion documents on 10 CPUs it took ~10h.

#### Train models

Once this step its completed a file `complete.txt.gz` will be created containg the processed corpus. Now it is possible to train the Word2Vec and the LSA models.

    python3 -m train.train_w2v --data <path to complete.txt.gz> --cores --save 
    python3 -m train.train_lsa --data <path to complete.txt.gz> --save 
    
Once again these steps are demanding in terms of computation. A special care is to be taken with the LSA model since it will load into RAM a matrix `V x V` where `V` is the size of the vocabulary to be retained from the corpus.
In order to avoid to have too many parameters to be set by default the vector dimension will be 300, 50000 tokens are kept from the source vocabulary and the window size is 10. If you wish to modify these values please edit the corresponding source code.

As a final note:
- word2vec model is SGNS (Skip-gram with Negative Sampling) with 20 negative samples
- lsa uses a cython module to speed up the coocurrence matrix construction. It is possible to use the scipy module (--raw-mode scipy) but results are not tested
- lsa use PPMI weighting before performing the Truncated SVD.
    

### Poincarè Embeddings

#### Generate transitive closure files

This step is for creating the training examples for the Poincarè embeddings.

    python3 -m utls.wordnet --pos (n,a,v,r) --out <out path>
    
For each of the available pos tags in wordnet a file containing the transitive closure of all hypernyms of a synset to file. This is done for nouns ann verbs. For adjectives and adverbs, which do not have a definition of hyperonym in WordNet a similarity function is used. Although this is not a 100% correct w.r.t. the assumption of the Poincarè model the hierachical relations for these pos tags seems to be accurate enough.

### Train Poincarè embeddings

For each of the possible pos tags a different embedding model is trained. 

    python3 -m utils.train_poincare --data <input> --save <out> --epochs --batch-size
    
Once again in order to avoid too many parameters to be selected the regularization term `C` is set to be 10, the vector size is 100, and 20 negative samples and 10 epochs of burn-in phase. If you wish to modify these values please edit the corresponding source code.

## Language model

In order to refine the candidates produced by the embeddings models a language model combined with a simple version of the beam search algorithm is used to produce the most likely sentence given the substituions.
To use the language model you will need to dowload one, for instance from [here](http://www.keithv.com/software/giga/).
    
Nontheless any language model in the `arpa` format will work.

Since to query the language model the [kenlm python interface](https://github.com/kpu/kenlm#python-module) is used, in order to speed up the loading of the language model you might want to use the [kenlm toolkit](https://kheafield.com/code/kenlm/) to build a binary file (see the documentation for a guide on how to do it).

## Postprocessing

Since all the vocabulary for each model is lemmatized once we are generating the subsitution we want the candidates to be in the same form as the original word. This means that verbs need to be conjugated and nouns pluralized/singularized. In order to do so the library [`pattern`](https://www.clips.uantwerpen.be/pattern) is exploited. At the time of writing this, the work for porting this library to `python3` is not completed yet. Therefore, installing this package via `pip` is discouraged.
The suggested way for now is to download the git development branch and install via `setup.py`
    
    git clone -b develpment https://github.com/pattern3/pattern
    python setup.py install
    
## Simplification

Once all of the above resources is ready it is possible to test the lexical semplification system.

For each word in a sentece its probability of occurring its computed. If it is below a given threshold (`--alpha` or `--simple-alpha` depending in which corpus is present) the systems decide to simplify or not.
If it does it uses the specified model to produce substitution candidates. It is possible to set the number of subsitution candidates to explore before filtering with `--topn`.

For Word2Vec or LSA candidates are filtered by:
- they do not share common lemma with original word
- are hyperonym or synonym of given word

For Poincarè embeddings:
- most similar in space
- if exploiting the word hierachy then ancestors and descendants of the given words are considered

All the candidates are scored with a simplified beam search algorithm: beams are expanded only for words which can be simplified ,i.e. they are noun/adjective/verb/adverbs and are rare words. It is possible to reduce the number of beams to be explored by setting the `--beam_width`.


    python3 main.py --dir <path where all models are stored> --model-type <w2v, lsa or poin> --mode <most_similar or hierachy>  --wiki-freq <folder for wiki_freq and simple_wiki_freq> --lm <langauge model>    

    
## Evaluation

In order to evaluate the Lexical Simpliifcation system it is possible to use the data set [BenchLS](http://www.lrec-conf.org/proceedings/lrec2016/summaries/155.html). In order to run the evaluation:

    python3 main.py --dir <path where all models are stored> --model-type <w2v, lsa or poin> --mode <most_similar or hierachy>  --wiki-freq <folder for wiki_freq and simple_wiki_freq> --lm <langauge model> --eval <BencLS>
    
This will evaluate the Subsitution Generation conmponent and the overall system performances as presented by [(4)](#references) .

## References

(1) Turney, Peter D., and Patrick Pantel. "From frequency to meaning: Vector space models of semantics." Journal of artificial intelligence research 37 (2010): 141-188.

(2) Mikolov, Tomas, et al. "Distributed representations of words and phrases and their compositionality." Advances in neural information processing systems. 2013.

(3) Nickel, Maximillian, and Douwe Kiela. "Poincaré embeddings for learning hierarchical representations." Advances in neural information processing systems. 2017.

(4) Paetzold, Gustavo, and Lucia Specia. "Lexenstein: A framework for lexical simplification." Proceedings of ACL-IJCNLP 2015 System Demonstrations (2015): 85-90.

## Requirements

- python3
- gensim >= 3.2
- pattern3 >= 2.6
- nltk > 3.3
- [kenlm](https://github.com/kpu/kenlm#python-module)

