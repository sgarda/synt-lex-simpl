#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 10:50:06 2018

@author: Samuele Garda
"""

import re
import logging
import pandas as pd
from utils.prepare_shootout import preprocess
from utils.postprocess import normalize

rm_rank = re.compile('[1-9]:')


logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO') 

def load_eval_data(path):
  """
  Load BenchLS.
  
  :param:
    path (str) : path to BenchLS
  
  :return:
    
    df (pandas.DataFrame) : parsed evaluation data set
  """
  
  data = {}
  
  with open(path) as infile:
    for idx,line in enumerate(infile):
      all_elements  = line.split('\t')
      data[idx] = {}
      data[idx]['sent'] = all_elements[0]
      data[idx]['c_w'] = all_elements[1]
      data[idx]['c_w_p'] = all_elements[2]
      data[idx]['sub'] = tuple([re.sub(rm_rank,'',x.strip())  for x in all_elements[3:]])
      
  df = pd.DataFrame.from_dict(data, orient='index')
  
  df.c_w_p.astype('int')
  
  logger.info("Loaded evaluation data set from `{}` with {} instances".format(path,len(data)))
  
  return df



class OverallEvaluation:
  
  @classmethod
  def precision(cls,target_word,gold,best_cand):
    """
    Consider hit if best simplification candidate is in gold standard or is the target word itself
    
    :param:
      target_word (str) : complex word
      gold (tuple) : gold standard simplifications
      best_cand (str) : first ranked simplification
      
    :return:
      score (int) : 1 if it is a hit 0 otherwise
    """
      
    score = 0
    
    if best_cand == target_word or best_cand in gold:
      
      score = 1
      
    return score
  
  @classmethod
  def accuracy(cls,target_word,gold,best_cand):
    """
    Consider hit if best simplification candidate is in gold standard and is not the target word 
    
    :param:
      target_word (str) : complex word
      gold (tuple) : gold standard simplifications
      best_cand (str) : first ranked simplification
      
    :return:
      score (int) : 1 if it is a hit 0 otherwise
    """
    
    score = 0
    
    if best_cand != target_word and best_cand in gold:
      
      score = 1
      
    return score
  
  @classmethod
  def change(cls,target_word,best_cand):
    """
    Check if substitution candidates is the word itself.
    
    :param:
      target_word (str) : complex word
      best_cand (str) : first ranked simplification
    
    :return:
      score (int) : 1 if it is a hit 0 otherwise
      
    """
    
    score = 0
    
    if best_cand == target_word:
      
      score = 1
      
    return score
  
  
class SubGenEval:
  
  @classmethod
  def potetianl(cls,sub,cand):
    
    return int(any(x in cand for x in sub))
  
  @classmethod
  def precision(cls,sub,cand):
    
    return sum([1 for s in sub if s in cand]) / len(sub)
  
  @classmethod
  def recall(cls,sub,cand):
    
    return sum([1 for c in cand if c in sub]) / len(cand)
  
  @classmethod
  def f1_score(cls,sub,cand):
    
    precision = cls.precision(sub,cand)
    recall = cls.recall(sub,cand)
    
    return  2 * (precision * recall / (precision + recall + 1e-7) ) 
  
  

def evaluate_lex_simpl(eval_path,simplifier,topn,lm):
  
  """
  Evaluate Lexical Simplification system with BenchLS. Compute Precision, Accuracy, Change
  
  :param:
    
    eval_path (str) : path to BenchLS
    simplifier (simplifier.LexicalSimplifier) : lexical Simplifier object
    topn (int) : number of recomendations 
    lm (kenlm.Model) : language model
    
  :return:
    precision,accuracy,change (int) : evaluation metrics
  """
  
  not_available = 0
  
  tot_prec,tot_acc,tot_change = 0,0,0
    

  actual = 0
  
  eval_data = load_eval_data(eval_path)
  
  for idx,test_case in eval_data.iterrows():
    
    try:
      
      actual += 1
    
      test_case_sent = test_case['sent']
      target_word = test_case['c_w']
      target_pos = int(test_case['c_w_p'])
      gold = test_case['sub']
            
      orig = test_case_sent.split()
      preprocessed = preprocess(test_case_sent, pre_tok = True)
    
      cands = simplifier.get_candidates(preprocessed[target_pos], topn = topn)
      
      cands = [normalize(target_word,c) for c in cands]
            
      all_hypotheses = []
      
      for c in cands:
        hypo = orig
        hypo[target_pos] = c
        all_hypotheses.append(' '.join(hypo))
        
      
      scored_hypotheses = [(hypo,lm.score(hypo, bos = False, eos = False)) for hypo in all_hypotheses]
      
      sorted_scored = sorted(scored_hypotheses , key=lambda tup: tup[1], reverse = True)
                  
      ranked = [sent_score[0].split()[target_pos] for sent_score in sorted_scored]
      
      best_cand = ranked[0]
            
      tot_prec += OverallEvaluation.precision(target_word,gold,best_cand)
      
      tot_acc += OverallEvaluation.accuracy(target_word,gold,best_cand)
      
      tot_change += OverallEvaluation.change(target_word,best_cand)
      
      
    except IndexError:
      
      not_available += 1
      
  
  print(not_available)      
  logger.info("Overall evaluation")
  logger.info("Precision : {} - Accuracy : {} - Chage : {}".format(tot_prec/actual,tot_acc/actual,tot_change/actual))
  


def evaluate_sub_gen(eval_path,simplifier,topn):
  
#Initialize variables: 
  potentialc = 0 
  potentialt = 0 
  precisionc = 0 
  precisiont = 0 
  recallt = 0
  
  not_available = 0
  
  eval_data = load_eval_data(eval_path)
  
  for idx,test_case in eval_data.iterrows():
    try:
      test_case_sent = test_case['sent']
      target_word = test_case['c_w']
      target_pos = int(test_case['c_w_p'])
      candidates = set(test_case['sub'])
          
      preprocessed = preprocess(test_case_sent, pre_tok = True)
      
      substitutions = simplifier.get_candidates(preprocessed[target_pos], topn = topn)
      
      substitutions = [normalize(target_word,c) for c in substitutions]
  
      overlap = candidates.intersection(set(substitutions)) 
      
      print(overlap)
  
      precisionc += len(overlap) 
      if len(overlap)>0:
        potentialc += 1 
      precisiont += len(substitutions) 
      potentialt += 1 
      recallt += len(candidates) 
    except IndexError:
      not_available += 1
 
  potential = float(potentialc)/float(potentialt) 
  precision = float(precisionc)/float(precisiont) 
  recall = float(precisionc)/float(recallt) 
  fmean = 0.0 
  if precision==0.0 and recall==0.0: 
    fmean = 0.0 
  else: 
    fmean = 2*(precision*recall)/(precision+recall) 
     
  #Return measures: 
  print(not_available)
  logger.info("Potential : {} - Precision : {} - Recall : {} - F1 score : {}".format(potential,precision,recall,fmean))
