#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 17:42:28 2018

@author: Samuele Garda
"""

import argparse
from utils.prepare_shootout import preprocess
from evaluation.evaluation import load_eval_data

def parse_arguments():
  """
  Parse command line arguments.
  """
  
  parser = argparse.ArgumentParser(description='Generate text with words to be kept in vocabulary generation for word embeddings models')
  parser.add_argument('--eval',required = True, type = str, help = "Path to BenchLS")
  parser.add_argument('--out',required = True, type = str, help = "Where to store word list")
  
  return parser.parse_args()

if __name__ == "__main__":
  
  args = parse_arguments()
  
  data = load_eval_data(args.eval)
  
  with open(args.out,'w+') as outfile:
    
    for idx,test_case in data.iterrows():
      
      processed = preprocess(test_case['sent'], pre_tok = True)
      
      target_pos = int(test_case['c_w_p'])
      
      word = processed[target_pos]
      
      outfile.write("{}\n".format(word))
      
      for w in test_case['sub']:
         try:
            outfile.write("{}\n".format(preprocess(w)[0]))
         except IndexError:
            pass
        
        
      
      
