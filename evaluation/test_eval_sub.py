#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 11:04:42 2018

@author: Samuele Garda
"""

def evaluate(eval_data,subs,simplifier,topn):
  
#Initialize variables: 
  potentialc = 0 
  potentialt = 0 
  precisionc = 0 
  precisiont = 0 
  recallt = 0
  
  eval_data = load_eval_data(eval_data)
  
  for idx,test_case in eval_data.iterrows():
    
    target = test_case['c_w']
    candidates = set(test_case['sub'])
    substitutions = simplifier.get_candidates(target, topn = topn)
    if target in substitutions: 
      overlap = candidates.intersection(set(substitutions[target])) 
      precisionc += len(overlap) 
      if len(overlap)>0:
        potentialc += 1 
      precisiont += len(substitutions[target]) 
    potentialt += 1 
    recallt += len(candidates) 
 
  potential = float(potentialc)/float(potentialt) 
  precision = float(precisionc)/float(precisiont) 
  recall = float(precisionc)/float(recallt) 
  fmean = 0.0 
  if precision==0.0 and recall==0.0: 
    fmean = 0.0 
  else: 
    fmean = 2*(precision*recall)/(precision+recall) 
     
  #Return measures: 
  return potential, precision, recall, fmean 
   